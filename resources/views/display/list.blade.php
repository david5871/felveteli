@extends("dashboard")

@section('title', 'List')
@section("content")

<div class="">
    <div class="container mt-5">
        @if (session('success'))
        <div class="alert alert-success" role="alert">
            <span class="">{{ session('success') }}</span>
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger" role="alert">
            <span class="">{{ session('error') }}</span>
        </div>
        @endif
    </div>

    {{-- connection error --}}
    @if(isset($errorMessage))
    <div class="alert alert-danger">
        {{ $errorMessage }}
    </div>
    @endif
    {{-- connection error end --}}

        <div class="container">
            <p class="text-center fs-3"><span class="fs-1 m-3">Hi!</span> {{ auth()->user()->name }}</p>
        </div>
        <a href="{{ route('create') }}">
          <button type="button" class="btn btn-dark text-success">Add company</button>
        </a>
        <div class="mt-2">
          @isset($companies)
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">taxNumber</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($companies as $company)
                  <tr >
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->taxNumber }}</td>
                    <td>
                      <a href="{{ route('company.showCompany', ['company' => $company]) }}">
                        <button type="button" class="btn btn-warning bg-warning">Details</button>
                    </a>
                    
                    </td>
                    
                  </tr>
                    @endforeach
                </tbody>
              </table>
              @else
              <p class="text-center">{{ $message }}</p>
          @endisset
        </div>
</div>
@endsection