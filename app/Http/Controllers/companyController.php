<?php

namespace App\Http\Controllers;

use App\Models\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class companyController extends Controller
{
    public function listCompanies()
    {
        try {
            // throw new \Exception('test');
            $companies = company::all();
            return view("display.list", ["companies" => $companies]);
            
        } catch (\Exception $e) {
            $errorMessage = 'Hiba történt az adatok lekérdezése során.';
            return view("display.list", ["message" => $errorMessage]);
        }
    }

    public function showCreate()
    {
        return view("display.create");
    }

    public function showCompany(company $company)
    {
        return view("display.company", ["company" => $company]);
    }

    public function editCompany(company $company)
    {
        return view("display.edit", ["company" => $company]);
    }

    public function updateCompany(Request $request, company $company)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'taxNumber' => 'required|max:8|min:8',
            'phone' => 'required|string|min:9|max:15',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $validatedData = $validator->validated();
            $company->name = $validatedData["name"];
            $company->taxNumber = $validatedData["taxNumber"];
            $company->phone = $validatedData["phone"];
            $company->email = $validatedData["email"];
            $company->save();
        } catch (\Exception $e) {
            return redirect()->route('list')->with('error', 'Hiba történt a frissítés során.');
        }

        return redirect()->route('list')->with('success', 'Cég sikeresen frissítve!');
    }

    public function destroyCompany(company $company)
    {
        try {
            $company->delete();
        } catch (\Exception $e) {
            return redirect()->route('list')->with('error', 'Hiba történt a törlés során.');
        }

        return redirect()->route('list')->with('success', 'Cég sikeresen törölve!');
    }

    public function createCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'taxNumber' => 'required|max:8|min:8',
            'phone' => 'required|string|min:9|max:15',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $validatedData = $validator->validated();

        try {
            $company = new company();
            $company->name = $validatedData["name"];
            $company->taxNumber = $validatedData["taxNumber"];
            $company->phone = $validatedData["phone"];
            $company->email = $validatedData["email"];
            $company->save();
        } catch (\Exception $e) {
            return redirect()->route('list')->with('error', 'Hiba történt a létrehozás során.');
        }

        return redirect()->route('list')->with('success', 'Cég sikeresen létrehozva.');
    }
}
